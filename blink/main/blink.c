/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"

/* Can use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO

void delay(uint32_t time);

void app_main(void)
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    gpio_reset_pin(BLINK_GPIO);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    while(1) {
        /* Blink off (output low) */
        printf("Turning off the LED with my delay\n");
        gpio_set_level(BLINK_GPIO, 0);
        delay(1000);
        //vTaskDelay(1000 / portTICK_PERIOD_MS);
        /* Blink on (output high) */
        printf("Turning on the LED with my delay\n");
        gpio_set_level(BLINK_GPIO, 1);
        delay(1000);
    }
}

void delay(uint32_t time)
{
	uint32_t i=0;
	uint32_t j=0;
	//printf("ticks ini %u\n",(uint32_t)xTaskGetTickCount());
	for(i=0;i<time;i++)
	{
		for(j=0;j<11403;j++)
		{
			__asm__("nop");
		}
	}
	//printf("ticks last %u\n",(uint32_t)xTaskGetTickCount());
}

